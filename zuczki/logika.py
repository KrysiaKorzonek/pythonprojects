from random import randint, sample


class Permutacja():
    def __init__(self, arg):
        if isinstance(arg, list):
            self.n = len(arg)
            self.permutacja = list(arg)
            return
        if isinstance(arg, Permutacja):
            self.n = arg.n
            self.permutacja = arg.permutacja.copy()
            return
        raise TypeError("niewlasciwa inicjalizacja permutacji")

    def __str__(self):
        return str(self.permutacja)

    def naloz_cykl(self, c):
        cykl = c.cykl
        elementy_obracane = [self.permutacja[i] for i in cykl]
        self.permutacja[cykl[0]] = elementy_obracane[-1]
        for nr_miejsca in range(1, len(cykl)):
            tu_ustawiamy = cykl[nr_miejsca]
            self.permutacja[tu_ustawiamy] = elementy_obracane[nr_miejsca-1]

    def __eq__(self, porownywana):
        if isinstance(porownywana, Permutacja):
            return self.permutacja == porownywana.permutacja
        else:
            return False

    def czy_id(self):
        return self.permutacja == list(range(self.n))

    def czy_trywialna(self, cykle):  # identyczność lub tylko 1 ruch
        if self.czy_id():
            return True
        for c in cykle:
            pom = Permutacja(self)
            pom.naloz_cykl(c)
            if pom.czy_id():
                return True
        return False

    def usun_samotne_wierzcholki(self, cykle):
        wierzch_na_cyklach = set()
        for wierzcholki in [c.cykl for c in cykle]:
            wierzch_na_cyklach.update(wierzcholki)
        nowy_numer = [-1]*self.n
        licznik = 0
        for i in range(self.n):
            if i in wierzch_na_cyklach:
                nowy_numer[i] = licznik
                licznik += 1
        nowa_permutacja = Permutacja(tlumacz_liste(self.permutacja, nowy_numer))
        nowe_cykle = [Cykl(tlumacz_liste(c.cykl, nowy_numer)) for c in cykle]
        return (nowa_permutacja, nowe_cykle)


def tlumacz_liste(stara_lista, lista_tlum):
    wynik = []
    for element in stara_lista:
        if lista_tlum[element] != -1:
            wynik.append(lista_tlum[element])
    return wynik


class Cykl():
    def __init__(self, arg):
        if isinstance(arg, list):
            tu_min = arg.index(min(arg))
            cykl_znormalizowany = arg[tu_min:] + arg[:tu_min]
            self.cykl = list(cykl_znormalizowany)
            return
        if isinstance(arg, Cykl):
            self.cykl = arg.cykl.copy()
            return
        raise TypeError("niewlasciwa inicjalizacja cyklu")

    def __str__(self):
        return str(self.cykl)

    def __eq__(self, porownywany):
        if isinstance(porownywany, Cykl):
            return self.cykl == porownywany.cykl
        else:
            return False

    def odwrotnosc(self):
        pierwszy = [self.cykl[0]]
        reszta = list(reversed(self.cykl[1:]))
        return Cykl(pierwszy + reszta)

    def dlug(self):
        return len(self.cykl)

    def krawedzie(self):
        wynik = []
        for i in range(self.dlug()-1):
            wynik.append((self.cykl[i], self.cykl[i+1]))
        wynik.append((self.cykl[-1], self.cykl[0]))
        return wynik


def losuj_element(tablica):
    losowy_indeks = randint(0, len(tablica)-1)
    return tablica[losowy_indeks]


# STAŁE - PARAMETRY POZIOMU TRUDNOŚCI

mozliwe_liczby_wierzcholkow = {
     1: [4, 5],
     2: [5, 6],
     3: [6, 7],
     4: [6, 7, 8],
     5: [7, 8, 9],
     6: [8, 9, 10]
}

mozliwe_liczby_cykli = {
     1: [2],
     2: [2, 3],
     3: [2, 3],
     4: [2, 3, 4],
     5: [2, 3, 4],
     6: [3, 4, 5]
}

mozliwe_liczby_ruchow = {
     1: [2, 3],
     2: [2, 3, 4],
     3: [3, 4, 5, 6],
     4: [3, 4, 5, 6, 7, 8],
     5: [5, 6, 7, 8, 9],
     6: [5, 6, 7, 8, 9, 10]
}

maksymalna_laczna_dlugosc_cykli = [0, 8, 10, 12, 15, 17, 20]


# GENEROWANIE PARAMETRÓW

def czy_jest_ustalonej_trudnosci(l_wierzch, l_cykli, l_ruchow, trudnosc):
    if l_wierzch not in mozliwe_liczby_wierzcholkow[trudnosc]:
        return False
    if l_cykli not in mozliwe_liczby_cykli[trudnosc]:
        return False
    if l_ruchow not in mozliwe_liczby_ruchow[trudnosc]:
        return False
    return True


# sprawdza, czy parametry o trudnosci trudnosc nie spełniają warunków
# dla trudnosci mala_trudnosc < trudnosc
# w takim razie nie traktujemy tego jako zagadki na poziomie trudnosc
def czy_jest_w_sam_raz_trudnosci(l_wierzch, l_cykli, l_ruchow, trudnosc):
    for tr in range(1, trudnosc):
        if czy_jest_ustalonej_trudnosci(l_wierzch, l_cykli, l_ruchow, tr):
            return False
    return True


def probuj_znalezc_parametry(trudnosc):
    liczba_wierzch = losuj_element(mozliwe_liczby_wierzcholkow[trudnosc])
    liczba_cykli = losuj_element(mozliwe_liczby_cykli[trudnosc])
    liczba_ruchow = losuj_element(mozliwe_liczby_ruchow[trudnosc])
    return (liczba_wierzch, liczba_cykli, liczba_ruchow)


# zwraca możliwe wartości w krotce
# odpowiednio: liczba wierzchołków, liczba cykli, liczba ruchów
def generuj_parametry(trudnosc):
    for i in range(5):
        parametry = probuj_znalezc_parametry(trudnosc)
        l_wierzch, l_cykli, l_ruchow = parametry
        if czy_jest_w_sam_raz_trudnosci(l_wierzch, l_cykli, l_ruchow, trudnosc):
            break
    return parametry


# GENEROWANIE CYKLI

def generuj_losowy_cykl(l_wierzch):
    dlug_cyklu = randint(2, l_wierzch)
    lista_cyklu = sample(list(range(l_wierzch)), dlug_cyklu)
    return Cykl(lista_cyklu)


def generuj_losowe_cykle(l_cykli, l_wierzch):
    wynik = []
    while len(wynik) < l_cykli:
        nowy_cykl = generuj_losowy_cykl(l_wierzch)
        if nowy_cykl not in wynik:
            wynik.append(nowy_cykl)
    return wynik


def generuj_losowe_cykle_odpowiedniej_trudnosci(l_cykli, l_wierzch, trudnosc):
    for i in range(10):
        proponowane_cykle = generuj_losowe_cykle(l_cykli, l_wierzch)
        suma_dlugosci = sum([c.dlug() for c in proponowane_cykle])
        if suma_dlugosci <= maksymalna_laczna_dlugosc_cykli[trudnosc]:
            break
    return proponowane_cykle


#   GENEROWANIE ZAGADKI

def generuj_permutacje_z_cykli(n, cykle, liczba_ruchow):
    liczba_cykli = len(cykle)
    odwrocone_cykle = [c.odwrotnosc() for c in cykle]
    for i in range(10):
        permutacja = Permutacja(list(range(n)))
        for j in range(liczba_ruchow):
            wylosowany_cykl = odwrocone_cykle[randint(0, liczba_cykli-1)]
            permutacja.naloz_cykl(wylosowany_cykl)
        if not permutacja.czy_trywialna(cykle):
            return permutacja
    return permutacja


def generuj_zagadke(trudnosc):  # trudnosc = 1, 2,..., 6
    l_wierzch, l_cykli, l_ruchow = generuj_parametry(trudnosc)
    cykle = generuj_losowe_cykle_odpowiedniej_trudnosci(l_cykli, l_wierzch, trudnosc)
    permutacja = generuj_permutacje_z_cykli(l_wierzch, cykle, l_ruchow)
    poprawione_parametry = permutacja.usun_samotne_wierzcholki(cykle)
    poprawiona_permutacja = poprawione_parametry[0]
    poprawione_cykle = poprawione_parametry[1]
    return (poprawiona_permutacja, poprawione_cykle)

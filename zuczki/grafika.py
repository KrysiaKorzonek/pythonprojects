from logika import Permutacja
from pygraphviz import *

kolory_pol = ["green", "blue", "yellow", "red", "turquoise2", "orange", "violetred1", "forestgreen", "darkorchid1", "honeydew3"]
kolory_zukow = ["green3", "blue3", "yellow2", "red3", "turquoise", "orange3", "violetred3", "darkgreen", "darkorchid3", "honeydew4"]

plik_graf_gry = "graf_gry.png"


def plik_graf_cyklu(nr_cyklu):
    return str(nr_cyklu) + "graf_cyklu.png"


def konstruuj_graf(l_wierzch, lista_cykli):
    G = AGraph(directed=True, strict=False, nodesep=0.3)
    G.node_attr['shape'] = 'circle'
    G.node_attr["label"] = " "
    G.node_attr['style'] = 'filled'
    G.node_attr['penwidth'] = 10
    G.edge_attr["style"] = "invisible"
    G.edge_attr["arrowhead"] = "none"
    for nr in range(l_wierzch):
        G.add_node(nr)
        wierz = G.get_node(nr)
        wierz.attr["color"] = kolory_pol[nr]
    for c in lista_cykli:
        G.add_cycle(c.cykl)
    return G


class Graf_Gry():
    def __init__(self, l_wierzch, lista_cykli):
        self.n = l_wierzch
        self.graf_gry = konstruuj_graf(self.n, lista_cykli)
        self.graf_gry.layout(prog="neato")

    def ustaw_zuki(self, permutacja):
        for (nr_pola, nr_zuka) in zip(range(self.n), permutacja.permutacja):
            v = self.graf_gry.get_node(nr_pola)
            v.attr["fillcolor"] = kolory_zukow[nr_zuka]

    def zapisz(self):
        self.graf_gry.draw(plik_graf_gry)


class Graf_Cyklu():
    def __init__(self, graf_wzor, cykl, nr):
        self.graf_cyklu = graf_wzor.graf_gry.copy()
        self.nr = nr
        for a, b in cykl.krawedzie():
            kraw = self.graf_cyklu.get_edge(a, b)
            kraw.attr["style"] = "solid"
            kraw.attr["arrowhead"] = "vee"
        self.zapisz_cykl()

    def zapisz_cykl(self):
        self.graf_cyklu.layout(prog="neato")
        self.graf_cyklu.draw(plik_graf_cyklu(self.nr))


class Gra():
    def __init__(self, per, lista_cykli):
        self.n = per.n
        self.permutacja_startowa = Permutacja(per)
        self.obecna_permutacja = Permutacja(per)
        self.plansza = Graf_Gry(self.permutacja_startowa.n, lista_cykli)
        self.lista_cykli = lista_cykli
        self.ruchy = []
        for nr, cykl in zip(range(len(lista_cykli)), lista_cykli):
            self.ruchy.append(Graf_Cyklu(self.plansza, cykl, nr))
        self.plansza.ustaw_zuki(self.obecna_permutacja)
        self.plansza.zapisz()

    def dzialaj_cyklem(self, cykl):
        self.obecna_permutacja.naloz_cykl(cykl)
        self.plansza.ustaw_zuki(self.obecna_permutacja)
        self.plansza.zapisz()

    def resetuj(self):
        self.plansza.ustaw_zuki(self.permutacja_startowa)
        self.obecna_permutacja = Permutacja(self.permutacja_startowa)
        self.plansza.zapisz()

    def czy_wygrana(self):
        return self.obecna_permutacja.czy_id()

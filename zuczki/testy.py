import unittest
from logika import *
from random import *


def losuj_element(tablica):
    losowy_indeks = randint(0, len(tablica)-1)
    return tablica[losowy_indeks]


parametry_trudnosci = [
    [4, 2, 3, 1],
    [5, 3, 2, 2],
    [5, 2, 4, 2],
    [5, 2, 2, 1],
    [6, 3, 8, 4],
    [8, 5, 5, 6]
    ]


class test_logiki(unittest.TestCase):
    def test_tworzenia_permutacji(self):
        """ tworzenie permutacji"""
        for i in range(10):
            lista = list(range(randint(2, 100)))
            self.assertEqual(Permutacja(lista).permutacja, lista)
            assert Permutacja(lista).czy_id()

    def test_kopiowania_permutacji(self):
        """kopiowanie permutacji"""
        for i in range(10):
            lista = list(range(randint(2, 100)))
            shuffle(lista)
            per = Permutacja(lista)
            per2 = Permutacja(per)
            self.assertEqual(per, per2)
            self.assertNotEqual(per, Permutacja(lista[1:]))
            per2.permutacja[0] += 1
            self.assertEqual(per.permutacja[0] + 1, per2.permutacja[0])

    def test_trywialnosci(self):
        """znajdowanie trywialnych permutacji"""
        for i in range(10):
            n = randint(2, 100)
            permutacja = Permutacja(list(range(n)))
            cykl = Cykl(sample(range(10), randint(1, 10)))
            assert permutacja.czy_trywialna([cykl])
            cykle = generuj_losowe_cykle(randint(1, 5), n)
            odwrocone = [c.odwrotnosc() for c in cykle]
            wylosowany = losuj_element(odwrocone)
            permutacja.naloz_cykl(wylosowany)
            assert permutacja.czy_trywialna(cykle)

    def test_usuwania_samotnych_wierzcholkow(self):
        """usuwanie samotnych wierzchołków"""
        for i in range(10):
            n = randint(2, 100)
            lista = list(range(n))
            shuffle(lista)
            per = Permutacja(lista)
            cyk = Cykl(list(range(n)))
            self.assertEqual((per, [cyk]), per.usun_samotne_wierzcholki([cyk]))
        for i in range(10):
            n = randint(3, 100)
            per = Permutacja(list(range(n)))
            cyk = Cykl(sample(range(n), n-2))
            per_po_usunieciu = per.usun_samotne_wierzcholki([cyk])[0]
            self.assertEqual(per_po_usunieciu, Permutacja(list(range(n-2))))

    def test_nakladania_cykli(self):
        """dzialanie na permutacje cyklem"""
        for i in range(10):
            n = randint(2, 100)
            lista = list(range(n))
            per = Permutacja(lista)
            cyk = Cykl(lista)
            per.naloz_cykl(cyk)
            per2 = Permutacja([n-1] + list(range(n-1)))
            self.assertEqual(per, per2)
        for i in range(10):
            n = randint(2, 100)
            lista = list(range(n))
            shuffle(lista)
            per = Permutacja(lista)
            per_backup = Permutacja(per)
            cykl = generuj_losowy_cykl(n)
            d = cykl.dlug()
            for j in range(d):
                per.naloz_cykl(cykl)
            self.assertEqual(per, per_backup)
        for i in range(10):
            n = randint(2, 100)
            lista = list(range(n))
            shuffle(lista)
            per = Permutacja(lista)
            per_backup = Permutacja(per)
            cykl = generuj_losowy_cykl(n)
            per.naloz_cykl(cykl)
            per.naloz_cykl(cykl.odwrotnosc())
            self.assertEqual(per, per_backup)

    def test_sprawdzania_trudnosci(self):
        """sprawdza czy parametry trudnosci dokladnie pasuja do poziomu"""
        for [wierz, cyk, ruch, trud] in parametry_trudnosci:
            assert czy_jest_ustalonej_trudnosci(wierz, cyk, ruch, trud)
            assert czy_jest_w_sam_raz_trudnosci(wierz, cyk, ruch, trud)

    def test_generowania_permutacji(self):
        """generowanie permutacji"""
        for i in range(10):
            n = randint(2, 100)
            cykle = generuj_losowe_cykle(5, n)
            per = generuj_permutacje_z_cykli(n, cykle, 2)
            czy_rozwiazane = False
            for cykl1 in cykle:
                for cykl2 in cykle:
                    per_testowa = Permutacja(per)
                    per_testowa.naloz_cykl(cykl1)
                    per_testowa.naloz_cykl(cykl2)
                    if per_testowa.czy_id():
                        czy_rozwiazane = True
            assert czy_rozwiazane


if __name__ == "__main__":
    unittest.main()

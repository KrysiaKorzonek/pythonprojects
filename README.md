# PythonProjects



## Gra "Żuczki" 

Celem gry jest doprowadzenie zagubionych żuczków na własciwe miejsca... albo, jeśli ktoś woli znalezienie takiego ciągu permutacji, który nałożony na permutację początkową da identyczność. 

Można grać na sześciu różnych poziomach - z pierwszym poziomem poradzi sobie nawet dziecko, szósty dostarczy wyzwań doświadczonym łamigłówkowiczom.

Pomysł na grę został zaczerpnięty z gry Matmoludki: Logiczna Podróż. 

Przykładowa gra na poziomie 3:

![przykładowa gra na poziomie 3](zuczki_poziom3.png)

Przykładowa gra na poziomie 6:

![przykładowa gra na poziomie 6](zuczki_poziom6.png)